require('dotenv').config();
const mongoose = require('mongoose');

const URI = process.env.MONGODB_URI ? process.env.MONGODB_URI_DOCKER : 'mongodb://admin:pass@localhost:27017/mern-app?authSource=admin';

mongoose.connect(URI, {
  useNewUrlParser: true,
  // useCreateIndex: true,
});

const { connection } = mongoose;

connection.once('open', () => {
  console.log('DB is connected');
});
