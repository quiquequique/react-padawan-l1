/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable class-methods-use-this */
import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export default class CreateNote extends Component {

  constructor(props) {

    super(props);

    this.state = {
      users: [],
      userSelected: '',
      title: 'title',
      content: 'content',
      usersUrl: 'http://localhost:4000/api/users/',
      notesUrl: 'http://localhost:4000/api/notes/',
      date: new Date(),
      editing: false,
      _id: '',
    };
  }

  componentDidMount() {
    this.getUsers();

    // eslint-disable-next-line react/destructuring-assignment
    const { params } = this.props.match;

    if (params.id) {
      this.getOneNote(params.id);
      this.setState({
        editing: true,
        _id: params.id,
      });
    }
  }

  getOneNote = async (id) => {

    const res = await axios.get(this.state.notesUrl + id);
    this.setState({
      userSelected: res.data.responce.author,
      title: res.data.responce.title,
      content: res.data.responce.content,
    });
  };

  getUsers = async () => {

    const { usersUrl } = this.state;

    const res = await axios.get(usersUrl);

    this.setState({
      users: res.data,
      userSelected: res.data[0].username,
    });

  };

  onSubmit = async (e) => {
    e.preventDefault();

    const {
      title,
      content,
      userSelected,
      date,
      notesUrl,
      editing,
      _id,
    } = this.state;
    const newNote = {
      title,
      content,
      author: userSelected,
      date,
    };
    if (editing) {
      await axios.put(notesUrl + _id, newNote);
    } else {
      await axios.post(notesUrl, newNote);
    }
    window.location.href = '/';

  };

  onInputChange = (e) => {

    this.setState({ [e.target.name]: e.target.value });

  };

  onChangeDate = (date) => {

    this.setState({ date });
  };

  render() {
    const {
      users,
      userSelected,
      date,
      title,
      content,
      editing,
    } = this.state;
    return (
      <div className="col-md-8 offset-md-2 my-3">
        <div className="card card-body">
          <h4>{!editing ? 'Create Note' : 'Edit Note'}</h4>
          <div className="form-group">
            <select name="userSelected" className="form-control" onChange={this.onInputChange} onClick={!editing ? this.onInputChange : undefined}>
              {
                users.map((user) => (
                  <option key={user.username} value={user.username}>
                    {editing ? userSelected : user.username}
                  </option>
                ))
              }
            </select>
            <h6 className="my-2 ml-2">{userSelected}</h6>
          </div>
          <div className="form-group">
            <input type="text" className="form-control" name="title" placeholder="title" value={editing ? title : ''} onChange={this.onInputChange} required />
          </div>
          <div className="form-group">
            <textarea name="content" id="content" className="form-control" placeholder="content" value={editing ? content : ''} onChange={this.onInputChange} required />
          </div>
          <div className="form-group">
            <DatePicker className="form-control" selected={date} onChange={this.onChangeDate} />
          </div>
          <form onSubmit={this.onSubmit}>
            <button type="submit" className="btn btn-secondary">
              {!editing ? 'Save' : 'Edit'}
            </button>
          </form>
        </div>
      </div>
    );
  }
}
