/* eslint-disable class-methods-use-this */
import axios from 'axios';
import React, { Component } from 'react';
import { format } from 'timeago.js';
import { Link } from 'react-router-dom';

export default class NotesLists extends Component {

  constructor(props) {
    super(props);
    this.state = {
      notesUrl: 'http://localhost:4000/api/notes/',
      notes: [],
    };
  }

  componentDidMount() {
    this.getNotes();
  }

  getNotes = async () => {
    const { notesUrl } = this.state;
    const res = await axios.get(notesUrl);
    // console.log(res);
    this.setState({ notes: res.data });
  };

  delCard = async (id) => {
    console.log(id);
    const { notesUrl } = this.state;
    await axios.delete(notesUrl + id);
    this.getNotes();
  };

  render() {
    const { notes } = this.state;
    return (
      <div className="row p-2">
        {
          notes.map((note) => (
            <div className="col-md-4 p-2" key={note._id}>
              <div className="card">
                <div className="card-header d-flex justify-content-between">
                  <h5>{note.title}</h5>
                  <Link type="button" className="btn btn-success" to={`/edit/${note._id}`}>
                    Edit
                  </Link>
                </div>
                <div className="card-body">
                  <p>
                    {note.content}
                  </p>
                  <p>
                    {note.author}
                  </p>
                  <p>
                    {format(note.date)}
                  </p>
                </div>
                <div className="card-footer">
                  <button type="button" className="btn btn-danger" onClick={() => this.delCard(note._id)}>
                    Delete
                  </button>
                </div>
              </div>
            </div>

          ))
        }
      </div>
    );
  }
}
