/* eslint-disable class-methods-use-this */
import React, { Component } from 'react';
import axios from 'axios';

export default class CreateUser extends Component {

  constructor(props) {

    super(props);

    this.state = {
      users: [],
      username: '',
      usersUrl: 'http://localhost:4000/api/users/',
    };
  }

  componentDidMount() {

    this.getUsers();

  }

  getUsers = async () => {

    const { usersUrl } = this.state;

    const res = await axios.get(usersUrl);

    this.setState({ users: res.data });

  };

  onChangeUsername = (e) => {

    this.setState({ username: e.target.value });

  };

  onSubmit = async (e) => {

    e.preventDefault();

    const { username, usersUrl } = this.state;
    await axios.post(usersUrl, {
      username,
    });

    this.setState({ username: '' });
    this.getUsers();

  };

  deleteUser = async (id) => {

    const { usersUrl } = this.state;
    await axios.delete(usersUrl + id);

    this.getUsers();
  };

  render() {
    const { users } = this.state;
    const { username } = this.state;
    return (
      <div className="row">
        <div className="col-md-4">
          <div className="card card-body">
            <h3>Create New User</h3>
            <form>
              <div className="form-group">
                <input type="text" className="form-control" onChange={this.onChangeUsername} value={username} />
                <button type="button" className="btn btn-primary my-2" onClick={this.onSubmit}>
                  Save
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="col-md-8">
          <ul className="list-group">
            {
              users.map((user) => (
                <li className="list-group-item list-group-item-action" key={user._id} onDoubleClick={() => this.deleteUser(user._id)}>
                  {user.username}
                </li>
              ))
            }
          </ul>
        </div>
      </div>
    );
  }
}
