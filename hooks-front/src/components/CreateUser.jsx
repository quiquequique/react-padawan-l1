/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/prop-types */
import React, { useEffect, useState, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import SimpleReactValidator from 'simple-react-validator';
import axios from 'axios';
import {
  Box,
  Input,
  FormControl,
  InputLabel,
  Paper,
  Button,
  Grid,
} from '@mui/material';

import usedConst from '../helpers/usedConst';

import UsersList from './UsersList';

const initialState = { username: '' };

export default function CreateUser() {

  const [users, setUsers] = useState();
  const [{ username }, setUsername] = useState(initialState);
  const simpleValidator = useRef(new SimpleReactValidator());
  const [, forceUpdate] = useState();

  useEffect(() => {
    if (!users) {
      fetchUsers();
    }
  }, []);

  const navigate = useNavigate();

  const clearState = () => {
    setUsername({ ...initialState });
  };

  const handleNameChange = (e) => {
    setUsername({ username: e.target.value });
  };

  const handleOnClick = async () => {
    const formValid = simpleValidator.current.allValid();

    if (!formValid) {
      simpleValidator.current.showMessages(true);
      forceUpdate(1);
    } else {
      console.log('submiting form');
      try {

        await axios.post(usedConst.USERS_URL, {
          username,
        });

        setTimeout(() => {
          clearState();
        }, 700);

        fetchUsers();

      } catch (error) {

        console.log(error);
        navigate('/error');
      }
    }
  };

  async function fetchUsers() {
    try {

      const res = await axios.get(usedConst.USERS_URL);
      const newArray = [...res.data];
      await setUsers(newArray);
      // console.log(users); /* _________________________________________________ Ask German !! */

    } catch (error) {
      console.log(error);
      navigate('/error');
    }

  }

  async function deleteUser(id) {
    try {

      await axios.delete(usedConst.USERS_URL + id);
      fetchUsers();

    } catch (error) {

      console.log(error);
      navigate('/error');

    }
  }

  return (
    <Box maxWidth="600px" margin="auto">
      <Paper
        elevation={4}
        sx={{
          marginTop: 6,
          backgroundColor: '#ff7961',
        }}
      >
        <Box
          component="form"
          sx={{ '& > :not(style)': { m: 1 } }}
          noValidate
          autoComplete="off"
          padding={2}
        >
          <FormControl variant="standard" sx={{ width: '100%' }}>
            <Grid
              container
              spacing={2}
            >
              <Grid
                item
                xs={8}
              >
                <InputLabel htmlFor="component-simple">
                  Add User
                </InputLabel>
                <Input
                  id="addUser"
                  value={username}
                  onChange={handleNameChange}
                  onBlur={() => {
                    simpleValidator.current.showMessageFor('username');
                    forceUpdate(1);
                  }}
                />
                {simpleValidator.current.message('username', username, 'required|alpha_space|min:5|max:20')}
              </Grid>
              <Grid
                item
                xs={4}
                marginBottom={0}
              >
                <Button
                  type="button"
                  variant="contained"
                  fullWidth
                  onClick={(e) => handleOnClick(e)}
                >
                  Add
                </Button>
              </Grid>
            </Grid>
          </FormControl>
        </Box>
      </Paper>
      {
        users && <UsersList users={users} deleteUser={deleteUser} />
      }
    </Box>
  );
}
