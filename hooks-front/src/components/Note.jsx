/* eslint-disable react/prop-types */
import React from 'react';
import { useNavigate } from 'react-router-dom';
import {
  Grid,
  Card,
  CardActions,
  CardContent,
  Button,
  Typography,
  Divider,
} from '@mui/material';
import { format } from 'timeago.js';

export default function Note(params) {

  const {
    note,
    deleteNote,
    setTrue,
  } = params;

  const navigate = useNavigate();

  const update = (id) => {
    setTrue();
    navigate(`/edit/${id}`);
  };

  return (
    <Grid item sm={6} lg={3}>
      <Card elevation={3}>
        <CardContent>
          <Typography sx={{ fontSize: 14 }} color="text.secondary">
            Created by:
          </Typography>
          <Typography variant="h5" component="div" gutterBottom>
            {note.author}
          </Typography>
          <Typography variant="h6" gutterBottom>
            {note.title}
          </Typography>
          <Typography variant="body2" gutterBottom>
            {note.content}
          </Typography>
          <Typography variant="body2" marginTop={3}>
            {format(note.date)}
          </Typography>
        </CardContent>
        <Divider variant="inset" component="li" sx={{ backgroundColor: '#757de8', height: '2px', margin: 1 }} />
        <CardActions>
          <Button
            size="small"
            onClick={() => update(note._id)}
          >
            Update
          </Button>
          <Button
            size="small"
            color="warning"
            onClick={() => deleteNote(note._id)}
          >
            Delete
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
}
