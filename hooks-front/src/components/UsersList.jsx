/* eslint-disable react/prop-types */
import React from 'react';

import { v4 as uuid } from 'uuid';
import {
  List,
  ListItem,
  Divider,
  ListItemAvatar,
  Avatar,
  Typography,
  Box,
  Paper,
} from '@mui/material';

export default function UsersList(props) {

  const {
    users,
    deleteUser,
  } = props;

  return (
    <Box>
      {users.length > 0
        ? (
          <List sx={{ width: '100%', bgcolor: 'background.paper', marginTop: 3 }}>
            {
              users.map((user) => (
                <Box key={uuid()}>
                  <Paper
                    elevation={2}
                    margin={5}
                  >
                    <ListItem
                      alignItems="flex-start"
                      sx={{ borderRadius: 2 }}
                      onDoubleClick={() => deleteUser(user._id)}
                    >
                      <ListItemAvatar>
                        <Avatar alt="Remy Sharp" sx={{ backgroundColor: '#757de8' }} key={uuid()} />
                      </ListItemAvatar>
                      <Typography
                        sx={{ display: 'inline', marginTop: 3 }}
                        component="span"
                        variant="body1"
                        color="text.primary"
                      >
                        {user.username}
                      </Typography>
                      <Typography
                        sx={{ display: 'inline', marginTop: 4, mx: 'auto' }}
                        component="span"
                        variant="body2"
                        color="text.primary"
                      >
                        {user._id}
                      </Typography>
                    </ListItem>
                  </Paper>
                  <Divider variant="inset" component="li" sx={{ backgroundColor: '#002984', height: 1, margin: 1 }} />
                </Box>
              ))
            }
          </List>
        ) : (
          <p>
            No users in the Database
          </p>
        )}
    </Box>
  );
}
