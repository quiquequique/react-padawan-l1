/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {
  useReducer,
  useState,
  useEffect,
  useRef,
  createRef,
} from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import SimpleReactValidator from 'simple-react-validator';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import {
  Box,
  Input,
  FormControl,
  InputLabel,
  Paper,
  Button,
  Typography,
  Select,
  TextareaAutosize,
  MenuItem,
} from '@mui/material';

import usedConst from '../helpers/usedConst';

const initialNoteState = {
  title: '',
  content: '',
  author: '',
  date: new Date(),
};

function reducer(state, { field, value }) {
  return {
    ...state,
    [field]: value,
  };
}

export default function CreateNote({ isEditingOneNote }) {

  const [users, setUsers] = useState();
  const [noteState, dispatch] = useReducer(reducer, initialNoteState);
  const simpleValidator = useRef(new SimpleReactValidator());
  const [, forceUpdate] = useState();

  const { id } = useParams();
  const form = createRef();

  useEffect(() => {
    if (isEditingOneNote) {
      editOneNote();
    }
  }, []);

  useEffect(() => {
    clearForm();
  }, [isEditingOneNote]);

  useEffect(() => {
    if (!users) {
      fetchUsers();
    }
  }, []);

  async function fetchUsers() {
    try {
      const res = await axios.get(usedConst.USERS_URL);
      const newArray = [...res.data];
      await setUsers(newArray);
      // console.log(users); /* ________________________________________________ Ask in review!! */

    } catch (error) {
      console.log(`exception: ${error}`);
      navigate('/');
    }
  }

  const onChange = (e) => {

    dispatch({ field: e.target.name, value: e.target.value });

  };

  const onChangeDate = (date) => {

    dispatch({ field: 'date', value: date });

  };

  const {
    author,
    title,
    content,
    date,
  } = noteState;

  const navigate = useNavigate();

  async function editOneNote() {
    try {

      const value = await axios.get(usedConst.NOTE_URL + id);
      dispatch({ field: 'author', value: value.data.responce.author });
      dispatch({ field: 'title', value: value.data.responce.title });
      dispatch({ field: 'content', value: value.data.responce.content });

    } catch (error) {

      console.log(`exception: ${error}`);
      navigate('/error');
    }
  }

  function clearForm() {
    dispatch({ field: 'author', value: '' });
    dispatch({ field: 'title', value: '' });
    dispatch({ field: 'content', value: '' });
  }

  const cleanFormAndRedirect = () => {

    setTimeout(() => {
      dispatch({ field: 'author', value: '' });
    }, 400);
    setTimeout(() => {
      dispatch({ field: 'title', value: '' });
    }, 650);
    setTimeout(() => {
      dispatch({ field: 'content', value: '' });
    }, 900);
    setTimeout(() => {
      dispatch({ field: 'date', value: new Date() });
      navigate('/');
    }, 1150);
  };

  const handleOnClick = async (e) => {
    e.preventDefault();

    const newNote = { ...noteState };

    const formValid = simpleValidator.current.allValid();

    if (!formValid) {
      simpleValidator.current.showMessages(true);
      forceUpdate(1);
    } else {
      console.log('submiting form');
      if (!isEditingOneNote) {
        try {

          await axios.post(usedConst.NOTE_URL, newNote);
          cleanFormAndRedirect();

        } catch (error) {

          console.log(`ecception: ${error}`);
          navigate('/error');

        }
      } else {
        try {

          await axios.put(usedConst.NOTE_URL + id, newNote);
          cleanFormAndRedirect();

        } catch (error) {

          console.log(`ecception: ${error}`);
          navigate('/error');

        }
      }
    }
  };

  return (
    <Box maxWidth="600px" margin="auto">
      {users
      && (
      <Paper
        elevation={3}
        sx={{
          marginTop: 5,
        }}
      >
        <Box
          component="form"
          sx={{ '& > :not(style)': { m: 1 } }}
          noValidate
          autoComplete="off"
          padding={2}
          ref={form}
        >
          <Typography
            variant="h4"
            noWrap
            component="h1"
            sx={{ flexGrow: 1, display: { xs: 'flex' } }}
          >
            Add a new task
          </Typography>
          <FormControl fullWidth variant="standard">
            <InputLabel variant="standard" id="select-label">
              Select User
            </InputLabel>
            <Select
              onChange={onChange}
              id="select"
              labelId="select-label"
              name="author"
              value={author}
              onBlur={() => {
                simpleValidator.current.showMessageFor('author');
                forceUpdate(1);
              }}
            >
              {
                users.map((user) => (
                  <MenuItem
                    value={user.username}
                    key={user._id}
                  >
                    {user.username}
                  </MenuItem>
                ))
              }
            </Select>
            {simpleValidator.current.message('author', author, 'required')}
          </FormControl>
          <FormControl variant="standard" sx={{ width: '100%' }}>
            <InputLabel htmlFor="component-simple">
              Title
            </InputLabel>
            <Input
              id="input-title"
              name="title"
              value={title}
              onChange={onChange}
              onBlur={() => {
                simpleValidator.current.showMessageFor('title');
                forceUpdate(1);
              }}
            />
            {simpleValidator.current.message('title', title, 'required|min:2|max:15|alpha_num_space')}
          </FormControl>
          <FormControl variant="standard" sx={{ width: '100%' }}>
            <TextareaAutosize
              name="content"
              aria-label="minimum height"
              minRows={3}
              placeholder="content"
              value={content}
              style={{ width: 'max-width' }}
              onChange={onChange}
              onBlur={() => {
                simpleValidator.current.showMessageFor('content');
                forceUpdate(1);
              }}
            />
            {simpleValidator.current.message('content', content, 'required|min:5|max:50|alpha_num_space')}
          </FormControl>
          <Typography sx={{ color: 'Light' }}>
            Date
          </Typography>
          <Box id="mark" variant="container-flex">
            <FormControl variant="standard">
              <DatePicker className="form-control" selected={date} onChange={onChangeDate} name="date" />
            </FormControl>
          </Box>
        </Box>
        <Button
          type="button"
          fullWidth
          variant="outlined"
          onClick={(e) => handleOnClick(e)}
        >
          {isEditingOneNote ? 'Edit Note' : 'Add Note'}
        </Button>
      </Paper>
      )}
    </Box>
  );
}
