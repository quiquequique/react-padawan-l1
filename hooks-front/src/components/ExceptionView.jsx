import React from 'react';
import { Typography } from '@mui/material';

export default function ExceptionView() {
  return (
    <div>
      <Typography variant="h4" component="h1" align="center" marginTop={3}>
        Please try again && || later
      </Typography>
      <img
        loading="lazy"
        src="https://media.istockphoto.com/vectors/website-construction-line-style-illustration-vector-id931042070"
        alt="under construction"
      />
    </div>
  );
}
