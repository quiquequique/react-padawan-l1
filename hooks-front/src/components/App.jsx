/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/jsx-no-bind */
import React, { useState, useEffect } from 'react';
import axios from 'axios';

import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

import '../css/App.css';

import {
  Container,
} from '@mui/material';

import usedConst from '../helpers/usedConst';
import ResponsiveAppBar from './ResponsiveAppBar';
import CreateNote from './CreateNote';
import CreateUser from './CreateUser';
import NotesList from './NotesList';
import ExceptionView from './ExceptionView';

export default function App() {

  const [notes, setNotes] = useState([]);
  const [isEditingOneNote, setIsEditingOneNote] = useState(false);

  useEffect(() => {
    if (notes.length <= 0) {
      getNotes();
    }
  }, []);

  async function getNotes() {
    try {

      const notesUrl = usedConst.NOTE_URL;
      const res = await axios.get(notesUrl);
      const resNotes = [...res.data];
      setNotes(resNotes);

    } catch (error) {

      console.log(`exception: ${error}`);
      window.location.href = '/error';

    }
  }

  const deleteNote = async (noteId) => {
    try {

      await axios.delete(usedConst.NOTE_URL + noteId);
      getNotes();

    } catch (error) {

      console.log(`exception: ${error}`);
      window.location.href = '/error';

    }
  };

  const setFalse = () => {
    setIsEditingOneNote(false);
  };

  const setTrue = () => {
    setIsEditingOneNote(true);
  };

  return (
    <div className="App">
      <Router>
        <ResponsiveAppBar setFalse={setFalse} />
        <main>
          <Container maxWidth="lg">
            <Routes>
              <Route
                path="/"
                exact
                element={(
                  <NotesList
                    notes={notes}
                    getNotes={getNotes}
                    deleteNote={deleteNote}
                    setTrue={setTrue}
                    isEditingOneNote={isEditingOneNote}
                  />
                )}
              />
              <Route path="/edit/:id" element={<CreateNote isEditingOneNote={isEditingOneNote} />} />
              <Route path="/create" element={<CreateNote isEditingOneNote={isEditingOneNote} />} />
              <Route path="/error" element={<ExceptionView />} />
              <Route
                path="/user"
                element={
                  (
                    <CreateUser />
                  )
                }
              />
            </Routes>
          </Container>
        </main>
      </Router>
    </div>
  );
}
