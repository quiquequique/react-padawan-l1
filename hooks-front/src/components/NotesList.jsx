/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import {
  Box,
  Grid,
  Typography,
  Paper,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import Note from './Note';

const LayoutContainer = styled('div')(() => ({
  height: '100%',
  overflow: 'hidden',
  width: '100%',
}));

export default function NotesList(params) {

  const {
    notes,
    getNotes,
    deleteNote,
    updateNote,
    setTrue,
  } = params;

  useEffect(() => {
    getNotes();
  }, []);

  return (
    <Box>
      <LayoutContainer>
        <Paper
          elevation={3}
          sx={{ marginTop: '30px', padding: '2px', backgroundColor: '#757de8' }}
        >
          <Typography variant="h4" marginTop={1} align="center">
            Listed tasks
          </Typography>
        </Paper>
        <Grid container spacing={2} marginTop={2} marginBottom={2}>
          {
            notes.map((note) => (
              <Note
                note={note}
                key={note._id}
                deleteNote={deleteNote}
                getNotes={getNotes}
                updateNote={updateNote}
                setTrue={setTrue}
              />
            ))
          }
        </Grid>
      </LayoutContainer>
    </Box>
  );
}
