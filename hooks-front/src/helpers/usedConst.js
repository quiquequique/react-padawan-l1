const usedConst = {
  NOTE_URL: 'http://localhost:4000/api/notes/',
  USERS_URL: 'http://localhost:4000/api/users/',
};

export default usedConst;
